//
//  FAUSounds.m
//  Truco
//
//  Created by Adolf on 13/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import "FAUSounds.h"
#import <AudioToolbox/AudioToolbox.h>


@implementation FAUSounds

+(void)playTouch{
    NSString *effect;
    NSString *type;
    effect = @"vareta1";
    type = @"mp3";
    SystemSoundID soundID;
    NSString *path = [[NSBundle mainBundle] pathForResource:effect ofType:type];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID ((__bridge_retained CFURLRef)url, &soundID);
    AudioServicesPlaySystemSound(soundID);
}
+(void)playClap{
    NSString *effect;
    NSString *type;
    effect = @"Palmas2";
    type = @"mp3";
    SystemSoundID soundID;
    NSString *path = [[NSBundle mainBundle] pathForResource:effect ofType:type];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID ((__bridge_retained CFURLRef)url, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

@end
