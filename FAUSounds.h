//
//  FAUSounds.h
//  Truco
//
//  Created by Adolf on 13/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAUSounds : NSObject

+(void)playTouch;
+(void)playClap;
@end
