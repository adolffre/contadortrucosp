//
//  FAUInfoTableViewController.m
//  Truco
//
//  Created by Adolf on 10/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import "FAUInfoTableViewController.h"
#import "FAULibrary.h"
#import "FXLabel.h"

@interface FAUInfoTableViewController ()

@end

@implementation FAUInfoTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *background = [UIImageView new];
    background.frame = self.tableView.frame;
    background.image = [UIImage imageNamed:@"bg.png"];
    background.alpha = 1;
    self.tableView.backgroundView = background;
    
    [self.tableView setBounces:NO];
    [self iniciarLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)iniciarLabels{
    
    [_pVersao label:_pVersao andSize:14];
    [_pCriadoPor label:_pCriadoPor andSize:14];
    [_pFeedBack label:_pFeedBack andSize:13];
    [_pCompartilhe label:_pCompartilhe andSize:13];
    [_pSobreNos label:_pSobreNos andSize:13];
    
    [_pBtnApps.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    [_pBtnAvalie.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    [_pBtnCompartilhar.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    [_pBtnEmail.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    [_pBtnSite.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    [_pBtnVoltar.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:13]];
    
    [_pAppNome setText:@"Truco Score - Paulista"];
    [_pAppNome label:_pAppNome andSize:16];
    [_pVersao setText: [NSString stringWithFormat:@"Versão %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]]];
    [_pVersao label:_pVersao andSize:12];
    [_pCriadoPor setText:@"Criado por Fauno's Tech"];
    
    [_pFeedBack setText:@"A sua opinião é muito importante para nós. Avalie o aplicativo na AppStore e envie sugestoes através de nosso site."];
    
    [_pCompartilhe setText:@"Se você gostou do aplicativo divulgue entre seus amigos!"];
    
    [_pSobreNos setText:@"Não deixe de conhecer nossos outros aplicativos e nosso trabalho através de  nosso site."];
    
}

- (IBAction)aBtnOk:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)aBtnAvalie:(id)sender {
    
    NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa";
    
    str = [NSString stringWithFormat:@"%@/wa/viewContentsUserReviews?", str];
    str = [NSString stringWithFormat:@"%@type=Purple+Software&id=", str];
    
    // Here is the app id from itunesconnect
    str = [NSString stringWithFormat:@"%@648624119", str];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    
    
}

- (IBAction)aBtnEmail:(id)sender {
    NSString *str = @"http://faunostech.com/contato.html";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)aBtnCompartilhar:(id)sender {
    
    NSMutableString *body = [NSMutableString string];
    
    [body appendString:@"<h1> Olá! </h1>"];
    [body appendString:@"<p>Encontrei esse novo aplicativo para ser o placar nos jogos de Truco! É leve, simples e fácil de usar.</p>"];
    [body appendString:@"<p>Para fazer o download na AppStore<a href=\"https://itunes.apple.com/pt/app/truco-sp/id648624119?mt=8/\">clique aqui</a>"];
    [body appendString:@"<p>Confere aí que vale a pena!</p>"];

    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"Olha que legal!"];
    [controller setMessageBody:body isHTML:YES];
    if (controller) [self presentModalViewController:controller animated:YES];
    
    
}

- (IBAction)aBtnApps:(id)sender {
    NSString *str = @"http://faunostech.com/apps.html";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)aBtnSite:(id)sender {
    NSString *str = @"http://faunostech.com/index.html";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}
- (void)viewDidUnload {
    [self setPVersao:nil];
    [self setPCriadoPor:nil];
    [self setPFeedBack:nil];
    [self setPCompartilhe:nil];
    [self setPSobreNos:nil];
    [self setPLabelCompartilhe:nil];
    [self setPLabelSobreNos:nil];
    [self setPBtnApps:nil];
    [self setPBtnEmail:nil];
    [self setPBtnCompartilhar:nil];
    [self setPBtnAvalie:nil];
    [self setPBtnApps:nil];
    [self setPBtnSite:nil];
    [self setPLabelFeedBack:nil];
    [self setPAppNome:nil];
    [self setPBtnVoltar:nil];
    [super viewDidUnload];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}



@end
