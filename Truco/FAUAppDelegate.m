//
//  FAUAppDelegate.m
//  Truco
//
//  Created by Adolf Jurgens on 05/12/12.
//  Copyright (c) 2012 HP. All rights reserved.
//

#import "FAUAppDelegate.h"
#import "iRate.h"
#import "FXTruco.h"
#import "FAULibrary.h"
#import "FXButton.h"

@implementation FAUAppDelegate

@synthesize window = _window;

+ (void)initialize
{
    //configure iRate
    [iRate sharedInstance].appStoreID =  648624119; // Replace this
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
}

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[FXTruco appearance] setTextColor:[FAULibrary colorWithHexString:@"#512d07"]];
    
    [[FXButton appearance] setTitleColor:[FAULibrary colorWithHexString:@"#512d07"] forState:UIControlStateNormal];
    [[FXButton appearance] setTitleColor:[FAULibrary colorWithHexString:@"#512d07"] forState:UIControlStateSelected];
    [[FXButton appearance] setTitleColor:[FAULibrary colorWithHexString:@"#512d07"] forState:UIControlStateHighlighted];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}
#pragma mark - Rotate fixed

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

@end
