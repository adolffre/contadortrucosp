

#import <UIKit/UIKit.h>

@interface FXLabel : UILabel

@property (nonatomic, assign) CGFloat shadowBlur;
@property (nonatomic, assign) CGSize innerShadowOffset;
@property (nonatomic, retain) UIColor *innerShadowColor;
@property (nonatomic, retain) UIColor *gradientStartColor;
@property (nonatomic, retain) UIColor *gradientEndColor;
@property (nonatomic, assign) CGPoint gradientStartPoint;
@property (nonatomic, assign) CGPoint gradientEndPoint;

-(void)label:(FXLabel *)_label andSize:(int)_size;


@end
