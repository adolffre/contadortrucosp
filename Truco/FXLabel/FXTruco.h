//
//  FXTruco.h
//  Truco
//
//  Created by Adolf on 10/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXTruco : UILabel


-(void)label:(FXTruco *)_label andSize:(int)_size;

@end
