//
//  FAUPlayer.m
//  Truco
//
//  Created by Adolf Jurgens on 05/12/12.
//  Copyright (c) 2012 HP. All rights reserved.
//

#import "FAUPlayer.h"

@implementation FAUPlayer

@synthesize name;
@synthesize points;
@synthesize bestOfThree;
@synthesize stars;

-(id)init{
    self = [super init];
    if(self!= nil){
        name = @"Jogador";
        points = 0;
        bestOfThree = 0;
        stars=0;
    }
    return self;
}

@end
