//
//  FAUPlayer.h
//  Truco
//
//  Created by Adolf Jurgens on 05/12/12.
//  Copyright (c) 2012 HP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAUPlayer : NSObject

@property(nonatomic,copy)NSString *name;
@property int points;
@property int bestOfThree;
@property int stars;

@end
