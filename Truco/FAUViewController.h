//
//  FAUViewController.h
//  Truco
//
//  Created by Adolf Jurgens on 05/12/12.
//  Copyright (c) 2012 HP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAUPlayer.h"
#import "FXLabel.h"
#import "FXTruco.h"


@interface FAUViewController : UIViewController <UIAlertViewDelegate>


- (IBAction)p1Button:(UIButton *)sender;
- (IBAction)p2Button:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet FXTruco *p1Name;
@property (strong, nonatomic) IBOutlet FXTruco *p2Name;
@property (strong, nonatomic) IBOutlet FXLabel *p1Points;
@property (strong, nonatomic) IBOutlet FXLabel *p2Points;
@property (strong, nonatomic) IBOutlet UIImageView *starP13;
@property (strong, nonatomic) IBOutlet UIImageView *starP12;
@property (strong, nonatomic) IBOutlet UIImageView *starP11;
@property (strong, nonatomic) IBOutlet UIImageView *starP23;
@property (strong, nonatomic) IBOutlet UIImageView *starP22;
@property (strong, nonatomic) IBOutlet UIImageView *starP21;
@property (weak, nonatomic) IBOutlet UIButton *pBtnZerar;
@property (weak, nonatomic) IBOutlet UIButton *pBtnInfo;
@property (weak, nonatomic) IBOutlet FXLabel *pPonto;
@property (weak, nonatomic) IBOutlet FXTruco *pTruco;
@property (weak, nonatomic) IBOutlet FXTruco *pSeis;
@property (weak, nonatomic) IBOutlet FXTruco *pNove;
@property (weak, nonatomic) IBOutlet FXTruco *pDoze;

- (IBAction)actionP1Name:(id)sender;
- (IBAction)actionP2Name:(id)sender;
- (IBAction)actionClean:(id)sender;

-(void)verifyCount;
-(UIImage *)star;
-(void)zeroScore;
-(void)putStar;
-(void)endGame;

@end
