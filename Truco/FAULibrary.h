//
//  FAULibrary.h
//  Truco
//
//  Created by Adolf Jurgens on 10/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAULibrary : NSObject

+(UIColor *)colorWithHexString:(NSString *)stringToConvert;

@end
