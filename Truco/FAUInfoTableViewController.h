//
//  FAUInfoTableViewController.h
//  Truco
//
//  Created by Adolf on 10/06/13.
//  Copyright (c) 2013 HP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "FXTruco.h"
#import "FXLabel.h"
@interface FAUInfoTableViewController : UITableViewController

- (IBAction)aBtnOk:(id)sender;
- (IBAction)aBtnAvalie:(id)sender;
- (IBAction)aBtnEmail:(id)sender;

- (IBAction)aBtnCompartilhar:(id)sender;
- (IBAction)aBtnApps:(id)sender;
- (IBAction)aBtnSite:(id)sender;
@property (weak, nonatomic) IBOutlet FXLabel *pAppNome;
@property (weak, nonatomic) IBOutlet FXTruco *pVersao;
@property (weak, nonatomic) IBOutlet FXTruco *pCriadoPor;
@property (weak, nonatomic) IBOutlet FXTruco *pFeedBack;
@property (weak, nonatomic) IBOutlet FXTruco *pCompartilhe;
@property (weak, nonatomic) IBOutlet FXTruco *pSobreNos;
@property (weak, nonatomic) IBOutlet FXLabel *pLabelFeedBack;
@property (weak, nonatomic) IBOutlet FXLabel *pLabelCompartilhe;
@property (weak, nonatomic) IBOutlet FXLabel *pLabelSobreNos;

@property (weak, nonatomic) IBOutlet UIButton *pBtnAvalie;
@property (weak, nonatomic) IBOutlet UIButton *pBtnEmail;
@property (weak, nonatomic) IBOutlet UIButton *pBtnCompartilhar;
@property (weak, nonatomic) IBOutlet UIButton *pBtnApps;
@property (weak, nonatomic) IBOutlet UIButton *pBtnSite;
@property (weak, nonatomic) IBOutlet UIButton *pBtnVoltar;


@end
