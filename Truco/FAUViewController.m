//
//  FAUViewController.m
//  Truco
//
//  Created by Adolf Jurgens on 05/12/12.
//  Copyright (c) 2012 HP. All rights reserved.
//

#import "FAUViewController.h"
#import "FAULibrary.h"
#import "FAUSounds.h"

@interface FAUViewController ()

@end

@implementation FAUViewController
{
    FAUPlayer *p1;
    FAUPlayer *p2;
    UIAlertView *_alertP1;
    UIAlertView *_alertP2;
    UIAlertView *_alertEndGame;

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_p1Points setFont:[UIFont fontWithName:@"ChunkFive" size:80]];
    [_p2Points setFont:[UIFont fontWithName:@"ChunkFive" size:80]];
    
    [_p1Name label:_p1Name andSize:17];
    [_p2Name label:_p2Name andSize:17];
    
    [_pBtnZerar.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:15]];
    [_pBtnZerar.titleLabel setTextColor:[FAULibrary colorWithHexString:@"#512d07"]];
    [_pBtnInfo.titleLabel setFont:[UIFont fontWithName:@"ChunkFive" size:15]];
    [_pBtnInfo.titleLabel setTextColor:[FAULibrary colorWithHexString:@"#512d07"]];


    _p1Points.innerShadowColor = [UIColor colorWithRed:(214/255) green:(180/255) blue:(117/255) alpha:0.8];
    _p1Points.innerShadowOffset= CGSizeMake(1.0f, 2.0f);

    _p2Points.innerShadowColor = [UIColor colorWithRed:(214/255) green:(180/255) blue:(117/255) alpha:0.8];
    _p2Points.innerShadowOffset= CGSizeMake(1.0f, 2.0f);
    
    
    p1 = [[FAUPlayer alloc]init];
    p2 = [[FAUPlayer alloc]init];
	[_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
    
    [_pTruco label:_pTruco andSize:20 ];
    [_pPonto label:_pPonto andSize:20];
    [_pSeis label:_pSeis andSize:20];
    [_pNove label:_pNove andSize:20];
    [_pDoze label:_pDoze andSize:20];
    

}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (IBAction)p1Button:(UIButton *)sender {
    [FAUSounds playTouch];

    if(sender.tag ==1){
        p1.points+=1;
        [_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    }
    else if(sender.tag ==2){
        p1.points+=3;
        [_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    }
    else if(sender.tag ==3){
        p1.points+=6;
        [_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    }
    else if(sender.tag ==4){
        p1.points+=9;
        [_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    }
    else {
        [p1 setPoints:12];
        [_p1Points setText:[NSString stringWithFormat:@"%d", p1.points]];
    } 
    [self verifyCount];
}

- (IBAction)p2Button:(UIButton *)sender {
    [FAUSounds playTouch];
    if(sender.tag ==1){
        p2.points+=1;
        [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
}
    else if(sender.tag ==2){
        p2.points+=3;
        [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
    }
    else if(sender.tag ==3){
        p2.points+=6;
        [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
    }
    else if(sender.tag ==4){
        p2.points+=9;
        [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
    }
    else {
        [p2 setPoints:12];
        [_p2Points setText:[NSString stringWithFormat:@"%d", p2.points]];
    }
    [self verifyCount];
}

- (IBAction)actionP1Name:(id)sender {
    _alertP1 = [[UIAlertView alloc] initWithTitle:@"Nome do Jogador 1"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Continue", nil];
    
    [_alertP1 setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [_alertP1 show];
}

- (IBAction)actionP2Name:(id)sender {
    _alertP2= [[UIAlertView alloc] initWithTitle:@"Nome do Jogador 2"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Continue", nil];
    
    [_alertP2 setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [_alertP2 show];

}

- (IBAction)actionClean:(id)sender {
    [self endGame];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= 3 && [inputText length]<9)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView == _alertP1 && buttonIndex!=0){
        p1.name = [[alertView textFieldAtIndex: 0] text];
        [_p1Name setText:[[alertView textFieldAtIndex:0]text]];
    }
    else if(alertView == _alertP2 && buttonIndex!=0){
        p2.name = [[alertView textFieldAtIndex: 0] text];
        [_p2Name setText:[[alertView textFieldAtIndex:0]text]];
    }
    
    else if(alertView == _alertEndGame){
        if(p1.bestOfThree ==3 || p2.bestOfThree==3){
            [self endGame];
            [self zeroScore];
        }
        else {
            [self zeroScore];
        }
    }
    
}

-(void)verifyCount{
    if (p1.points >11) {
        [_p1Points setText:[NSString stringWithFormat:@"%d", 12]];
        [self putStar];
    }
    
    else if(p2.points >11) {
        [_p2Points setText:[NSString stringWithFormat:@"%d", 12]];
        [self putStar];
        
    }
    
}

-(void)putStar{
       if(p1.points >11){
        p1.bestOfThree+=1;
        
        if(p1.stars==0){
            [_starP11 setImage:[self star]];
            [self alertWinner:p1];
            p1.stars++;
        }
        else if(p1.stars==1){
            [_starP12 setImage:[self star]];
            [self alertWinner:p1];
            p1.stars++;
        }
        else {
            [_starP13 setImage:[self star]];
            [self alertWinner:p1];
        }
    }
    else if(p2.points >11){
        p2.bestOfThree+=1;
        if(p2.stars==0){
            [_starP21 setImage:[self star]];
            [self alertWinner:p2];
            p2.stars++;
        }
        else if(p2.stars==1){
            [_starP22 setImage:[self star]];
            [self alertWinner:p2];
            p2.stars++;
        }
        else {
            [_starP23 setImage:[self star]];
            [self alertWinner:p2];
        }
    }
    
}


-(void)zeroScore{
    [_p1Points setText:[NSString stringWithFormat:@"%d", 0]];
    [_p2Points setText:[NSString stringWithFormat:@"%d", 0]];
    p1.points = 0;
    p2.points = 0;
}



-(UIImage *)star{
    UIImage *image = [UIImage imageNamed: @"star2.png"];
    return image;
}

-(void)fauplayer:(UIViewController *)view salvaJogadores:(NSArray *)jogadores{

    if([[jogadores objectAtIndex:0]length]>0 ){
        [p1 setName:[jogadores objectAtIndex:0]];
        [_p1Name setText:[p1 name]];
    }
    if([[jogadores objectAtIndex:1]length]>0 ){
        
        [p2 setName:[jogadores objectAtIndex:1]];
        [_p2Name setText:[p2 name]];
    }
    [view dismissModalViewControllerAnimated:YES];
    

}
-(void)alertWinner:(FAUPlayer *)_winner{
    NSString *winner = [[NSString alloc]initWithFormat:@"%@ Ganhou!",_winner.name];
    
    if(_winner.bestOfThree ==3){
        
        [FAUSounds playClap];
        
       UIAlertView *endGame = [[UIAlertView alloc] initWithTitle: @"Parabéns! Melhor de 3!" message: winner delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil]; 
        [endGame show];
    }
    else{
    
    _alertEndGame = [[UIAlertView alloc] initWithTitle: @"Parabéns! ! !" message: winner delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [_alertEndGame show];
    }
        
}


-(void)endGame{
    UIImage *noStar = [UIImage imageNamed: @"star1.png"];
    [_starP21 setImage:noStar];
    [_starP22 setImage:noStar];
    [_starP23 setImage:noStar];
    [_starP11 setImage:noStar];
    [_starP12 setImage:noStar];
    [_starP13 setImage:noStar];
    p1.points = 0;
    p2.points = 0;
    p1.bestOfThree = 0;
    p2.bestOfThree = 0;
    p1.stars=0;
    p2.stars =0;
    _p1Points.text = @"0";
    _p2Points.text = @"0";
}



@end
